#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/types.h"
#include "string.h"

#define TBUF_SIZE (10U)

int main(int argc, char ** argv)
{
	int not_my_fd[2];
	char tbuf[TBUF_SIZE] = "batman\n";

	if(argc < 2)
	{
		printf("err, requires argument : write fd of the other process\n");
		exit(0);
	}
	else
	{
		not_my_fd[0] = 0;
		not_my_fd[1] = atoi(argv[1]);
		printf("write fd %d\n", not_my_fd[1]);
	}

	printf("writing to pipe with fd[1] %d\n", not_my_fd[1]);
	while(1)
	{
		if( 0 > write(not_my_fd[1], tbuf, TBUF_SIZE))
		{
			printf("write to fd failed\n");
			exit(0);
		}
		else
		{
			sleep(3);
		}
	}
	return (0);
}