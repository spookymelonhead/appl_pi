#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/types.h"
#include "string.h"

#define RBUF_SIZE (10U)

int main(int argc, char ** argv)
{
	int my_fd[2];
	char rbuf[RBUF_SIZE];

	pipe(my_fd);
	printf("pipe created with read: %d and write: %d\n", my_fd[0], my_fd[1]);
	while(RBUF_SIZE != read(my_fd[0], rbuf, RBUF_SIZE));
	printf("rbuf: %s\n", rbuf);
	return (0);
}