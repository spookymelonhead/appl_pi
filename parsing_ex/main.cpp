#include <iostream>

#include "stdio.h"

#include "stdint.h"

#include "stdlib.h"

#include "string.h"


using namespace std;


void print_array(uint8_t length, uint8_t *dptr);



#define API_FRAME_SIZE ( API_FRAME_DELIMETER_SIZE  							+ 
											   API_FRAME_LENGTH_SIZE     							+
											   API_FRAME_FRAME_TYPE_SIZE 							+ 
											   FRAME_DATA_FRAME_ID_SIZE  							+
											   FRAME_DATA_DESTINATION_ADDRESS_SIZE_16 +
											   FRAME_DATA_TRANSMIT_OPTIONS_SIZE )

#define API_FRAME_DELIMETER_SIZE                          (1U)

#define API_FRAME_LENGTH_SIZE                             (2U)

#define API_FRAME_FRAME_TYPE_SIZE                         (1U)

#define FRAME_DATA_FRAME_ID_SIZE                          (1U)

#define FRAME_DATA_DESTINATION_ADDRESS_SIZE_16            (2U)

#define FRAME_DATA_TRANSMIT_OPTIONS_SIZE                  (1U)

    #define DATA_SIZE (HEADER_SIZE + DATA_PROTOCOL_VERSION_SIZE + DATA_LENGTH_SIZE + DATA_SOURCE_ADDRESS_SIZE + DATA_DESTINATION_ADDRESS_SIZE + DATA_PACKET_TYPE_LENGTH + DATA_SYNC_COMMAND_SIZE )
        
        #define HEADER_SIZE                                              (3U)
  
        #define DATA_PROTOCOL_VERSION_SIZE                               (1U)

        #define DATA_LENGTH_SIZE                                         (2U)
      
        #define DATA_SOURCE_ADDRESS_SIZE                                 SOURCE_ADDRESS_64_BIT_SIZE

        #define DATA_DESTINATION_ADDRESS_SIZE                            DESTINATION_ADDRESS_64_BIT_SIZE

				#define DATA_PACKET_TYPE_LENGTH																	(2U)

        #define DATA_SYNC_COMMAND_SIZE                                   (1U)


#define ZIGBEE_API_START_DELIMTER (0x7EU)

#define ZIGBEE_TRANSMIT_REQUEST_FRAME_TYPE (0x10U)

#define ZIGBEE_FRAME_ID (0x01U)

#define ZIGBEE_BROADCAST_RADIUS (0x00U)

#define ZIGBEE_TRANSMIT_OPTIONS (0x00)

#define HEADER_0 (0xFFU)
#define HEADER_1 (0xFFU)
#define HEADER_2 (0xFDU)

#define PROTOCOL_VERSION (0x01U)

const uint8_t header[HEADER_SIZE] = {0xFF, 0xFF, 0xFD};

const uint8_t destination_address_64_bit_be[DESTINATION_ADDRESS_64_BIT_SIZE] = {0x00, 0x13, 0xA2, 0x00, 0x41, 0x5A, 0xD1, 0xE5};

const uint8_t destination_address_64_bit_le[DESTINATION_ADDRESS_64_BIT_SIZE] = {0xE5, 0xD1, 0x5A, 0x41, 0x00, 0xA2, 0x13, 0x00};

const uint8_t destination_address_16_bit_be[DESTINATION_ADDRESS_16_BIT_SIZE] = {0xFF, 0xFE}; 

const uint8_t source_address_64_bit_be[SOURCE_ADDRESS_64_BIT_SIZE] = {0x00, 0x13, 0xA2, 0x00, 0x41, 0x5A, 0xD2, 0x15};

const uint8_t source_address_64_bit_le[SOURCE_ADDRESS_64_BIT_SIZE] = {0x15, 0xD2, 0x5A, 0x41, 0x00, 0xA2, 0x13, 0x00};

typedef struct 
{
  union
  {
    uint8_t packet_frame[PACKET_FRAME_SIZE];

  	struct
    {
      union
      {
        uint8_t api_frame[API_FRAME_SIZE];

        struct
        {
          uint8_t api_delimiter;

          union
          {
            uint8_t length[API_FRAME_LENGTH_SIZE];

            struct
            {
              uint8_t length_msb;

              uint8_t length_lsb;

            }length_s;

          }length_u;

          uint8_t api_frame_type;

          uint8_t frame_id;

          uint8_t destination_address_16[2];

          uint8_t transmit_options;

        }api_frame_s;

      }api_frame_u;

      union
      {
        uint8_t data[DATA_SIZE];

        struct
        {
          uint8_t header[HEADER_SIZE];

          uint8_t left_horizontal_jotstick;

					uint8_t left_vertical_jotstick;

					uint8_t right_horizontal_jotstick;

					uint8_t left_vertical_jotstick;

					uint8_t analog_knob_one;

					uint8_t analog_knob_two;

					uint8_t end_of_packet;

        }data_s;

      }data_u;

      uint8_t crc_byte;

    }packet_frame_s;

  }packet_frame_u;
  
}zigbee_transmit_req_packet_t;


int main()
{
	zigbee_transmit_req_packet_t tr_packet;

	printf("\nTest begins..");

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.api_delimiter = ZIGBEE_API_START_DELIMTER;

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.length_u.length_s.length_msb = (0x002F >> 8U) & 0xFF;

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.length_u.length_s.length_lsb = (0x002F >> 0U) & 0xFF;

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.api_frame_type = ZIGBEE_TRANSMIT_REQUEST_FRAME_TYPE;

	tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.frame_id = ZIGBEE_FRAME_ID;

	memcpy(tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64, destination_address_64_bit_be, DESTINATION_ADDRESS_64_BIT_SIZE); 	

	memcpy(tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_16, destination_address_16_bit_be, DESTINATION_ADDRESS_16_BIT_SIZE);
	
  tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.broadcast_radius = ZIGBEE_BROADCAST_RADIUS;
	tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.transmit_options = ZIGBEE_TRANSMIT_OPTIONS;

	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header[0] = HEADER_0;
	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header[1] = HEADER_1;
	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header[2] = HEADER_2;

	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.protocol_version = PROTOCOL_VERSION;

	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.length_u.length_s.length_lsb = 0x1b;

  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.length_u.length_s.length_msb = 0x00;


  memcpy(tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.source_address_64, source_address_64_bit_le, SOURCE_ADDRESS_64_BIT_SIZE);
  memcpy(tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.destination_address_64, destination_address_64_bit_le, DESTINATION_ADDRESS_64_BIT_SIZE);

  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.packet_type_u.packet_type_s.packet_type_lsb = 0xC8;
  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.packet_type_u.packet_type_s.packet_type_msb = 0x00;

  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.sync_command = 0x01;

	//printf("\nFRAME DATA Destination address 64 : ");
	//print_array(8, &tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64[0]);

	printf("\n");
	print_array(PACKET_FRAME_SIZE, &tr_packet.packet_frame_u.packet_frame[0]);
	printf("\n");

}



void print_array(uint8_t length, uint8_t *dptr)
{
	uint8_t i;

	for(uint8_t i = 0U; i<length; i++)
	{
		printf("  %x", *dptr);
		dptr++;
	}
}


void big_edian_to_little_endian()
{

}


#if 0
	zigbee_transmit_req_packet_t tr_packet = {0x7E, 0x00, 0x2F, 0x10,
																						0x01, 0x00, 0x13, 0xa2, 0x00, 0x41, 0x5a, 0xd1, 0xe5, 0xFF, 0xFE, 0x69, 0x69,
	 																					
	 																					0xFF, 0xFF, 0xFD,
																					  0x01, 0x1B, 0x00,
																					  0x00, 
																					  0x15, 0xD2, 0x5A, 0x41, 0x00, 0xA2, 0x13, 0x00, 
																					  0xE5, 0xD1, 0x5A, 0x41, 0x00, 0xA2, 0x13, 0x00, 
																					  0xC8, 0x00 };


	printf("\nPrinting variable by variable	");

	printf("\nAPI Delimter: %x",tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.api_delimiter );
	printf("\nAPI LENGTH_MSB: %x",tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.length_u.length_s.length_msb );
	printf("\nAPI LENGTH_LSB: %x",tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.length_u.length_s.length_lsb );
	printf("\nAPI FRAME type: %x",tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.api_frame_type );

	printf("\nFRAME DATA FRAME ID: %x",tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.frame_id );	

	printf("\nFRAME DATA Destination address 64 : %lx", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64_u.destination_address_64);
	
	printf("\nFRAME DATA Destination address 64 MSB  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64_u.destination_address_64_s.destination_address_64_msb);
	printf("\nFRAME DATA Destination address 64 LSB  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64_u.destination_address_64_s.destination_address_64_lsb);

	printf("\nFRAME DATA Destination address  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_16_u.destination_address_16 );
	printf("\nFRAME DATA Destination address MSB  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_16_u.destination_address_16_s.destination_address_16_msb);
	printf("\nFRAME DATA Destination address LSB  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_16_u.destination_address_16_s.destination_address_16_lsb);

	printf("\nFRAME DATA Broadcast radius  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.broadcast_radius);
	printf("\nFRAME DATA Transmit options  : %x", tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.transmit_options);

	printf("\nCRONET DATA Header  : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header[0]);
	printf("\nCRONET DATA Header  : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header[1]);
	printf("\nCRONET DATA Header  : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header[2]);

	printf("\nCRONET DATA Protocol Version  : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.protocol_version);


	printf("\nCRONET DATA Length : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.length_u.length);
	//printf("\nAPI FRAME DATA Protocol Version  : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.protocol_version);
	//printf("\nAPI FRAME DATA Protocol Version  : %x", tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.protocol_version);	




	printf("\n\n\nsize of:	%u", sizeof(zigbee_transmit_req_packet_t));

	printf("\n");

	print_array(PACKET_FRAME_SIZE, &tr_packet.packet_frame_u.packet_frame[0]);

	tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64_u.destination_address_64_s.destination_address_64_msb = 0x0013a200;

#endif





device_return_e zigbee_create_transmit_request_packet(zigbee_transmit_req_packet_t * tr_packet, uint8_t * payload_data_ptr, uint32_t payload length, uint8_t sync_comand,  )
{

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.api_delimiter = ZIGBEE_API_START_DELIMTER;

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.length_u.length_s.length_msb = (0x002F >> 8U) & 0xFF;

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.length_u.length_s.length_lsb = (0x002F >> 0U) & 0xFF;

	tr_packet.packet_frame_u.packet_frame_s.api_frame_u.api_frame_s.api_frame_type = TRANSMIT_REQUEST_FRAME_TYPE;

	tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.frame_id = ZIGBEE_FRAME_ID;

	memcpy(tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_64, destination_address_64_bit_be, DESTINATION_ADDRESS_64_BIT_SIZE); 	

	memcpy(tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.destination_address_16, destination_address_16_bit_be, DESTINATION_ADDRESS_16_BIT_SIZE);
	
  tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.broadcast_radius = ZIGBEE_BROADCAST_RADIUS;
	tr_packet.packet_frame_u.packet_frame_s.frame_data_u.frame_data_s.transmit_options = ZIGBEE_TRANSMIT_OPTIONS;

	memcpy(tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.header, header, HEADER_SIZE);

	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.protocol_version = PROTOCOL_VERSION;

	tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.length_u.length_s.length_lsb = 0x1b;

  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.length_u.length_s.length_msb = 0x00;


  memcpy(tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.source_address_64, source_address_64_bit_le, SOURCE_ADDRESS_64_BIT_SIZE);
  memcpy(tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.destination_address_64, destination_address_64_bit_le, DESTINATION_ADDRESS_64_BIT_SIZE);

  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.packet_type_u.packet_type_s.packet_type_lsb = 0xC8;
  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.packet_type_u.packet_type_s.packet_type_msb = 0x00;

  tr_packet.packet_frame_u.packet_frame_s.data_u.data_s.sync_command = 0x01;

}