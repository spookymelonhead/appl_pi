/*
 * CRON Systems Pvt Ltd LLC "CRON Systems" Confidential
 * Copyright (c) 2014 - 2025 CRON Systems Pvt Ltd. All Rights Reserved
 *
 * NOTICE : All information contained herein is, and remains the property of CRON Systems Pvt Ltd.
 * The intellectual and technical concepts contained herein are proprietary to CRON Systems Pvt Ltd
 * and may be covered by the Foreign Patents, patents in progess, and are protected by
 * trade secret and copyright law.
 *
 * Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from CRON Systems Pvt Ltd. Access to the source code
 * contained herein is hereby forbidden to anyone except current permanent CRON Systems Pvt Ltd employees,
 * managers and directors who have executed Confidentiality and Non-disclosure agreements
 * explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or
 * disclosure  of this source code, which includes information that is confidential and/or
 * proprietary, and is a trade secret of CRON Systems Pvt Ltd. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF CRON Systems Pvt Ltd IS STRICTLY PROHIBITED,
 * AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  TO REPRODUCE,
 * DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT
 * MAY DESCRIBE, IN WHOLE OR IN PART.
 *
 */

/*
  @file
  cron_config_zigbee_application.h

  @author
  Hardik Madaan

  @created on
  02/09/2017

  @brief
  This file has config for zigbee applicaiton
  
  @details
  This file has config for zigbee application
  
*/

/*==============================================================================
                           REVISION HISTORY
==============================================================================*/


#ifndef CRON_CONFIG_ZIGBEE_APPLICATION_H_
#define CRON_CONFIG_ZIGBEE_APPLICATION_H_

/*==============================================================================
                                INCLUDE FILES
==============================================================================*/

#include "device_std_types.h"

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : MACROS
==============================================================================*/

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : ENUMS
==============================================================================*/

/*==============================================================================
                    LOCAL DEFINITIONS AND TYPES : STRUCTURES
==============================================================================*/

/*----------------------------------------------------------------------------*/
/*!@brief struct to define the CAN BIT config */
typedef struct
{
  /*!> Config to check if can bit is configurable */
  gio_device_bit_configurability_e is_bit_configurable;

  /*!> CAN initial output value */
  //gio_device_initial_output_values_e initial_output_values;

  /*!> If output direction of CAN is enabled */
  gio_device_output_direction_enable_e output_direction_enable;

  /*!> If open drain output of CAN is enabled */
  gio_device_open_drain_output_enable_e open_drain_output_enable;

  /*!> Pull up pull down select for CAN */
  gio_device_pull_up_pull_down_select_e pull_up_pull_down_select;

  /*!> Pull up pull down enable for CAN */
  gio_device_pull_up_pull_down_enable_e pull_up_pull_down_enable;

}cron_config_can_bit_s;


/*----------------------------------------------------------------------------*/
/*!@brief struct to define the CAN config per port */
typedef struct
{
  /*!> Cron Config for enable GIO mfunctionality in CAN*/
  can_device_gio_mode_e gio_mode;

  /*!> Cron Config for CAN bits */
  cron_config_can_bit_s cron_config_can_bit [CAN_DEVICE_BIT_NUM];
  
}cron_config_can_per_device_s;

/*----------------------------------------------------------------------------*/
/*!@brief struct to define the CAN config */
typedef struct
{
  /*!> Cron Config for CAN bits */
  cron_config_can_per_device_s cron_config_can_per_device[CAN_DEVICE_NUM];
  
}cron_config_can_s;

/*==============================================================================
                              EXTERNAL DECLARATIONS
=============================================================================*/

extern cron_config_zigbee_application_s cron_config_zigbee_application;

/*==============================================================================
                           EXTERNAL FUNCTION PROTOTYPES
==============================================================================*/

#endif /* CRON_CONFIG_ZIGBEE_APPLICATION_H_ */

