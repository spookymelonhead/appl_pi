#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

typedef enum
{
	beta	=0U,
	alpha,
	gamma,

	INVALID

}jack_type_e;

typedef struct
{
	bool state;
	struct jack_info_s * head_jack;
	union
	{
		int weight_int;
		float weight_float;
	};

}jack_state_per_device;

typedef struct
{
	jack_state_per_device[3];
}jack_state;

struct jack_info_s
{
	float timestamp;
	jack_type_e jack_type;
	struct jack_info_s * next_jack;

};

void some_funk();

int main(int argc, char **argv)
{
	some_funk();

	return 0;
}

void some_funk()
{
	jack_state *jack = NULL; 

	jack->jack_state_per_device = ( jack_state_per_device * ) malloc ( sizeof ( jack_state_per_device ) );
	memset((void *) jack_state_per_device, 0x0,(sizeof(struct jack_state_per_device) ) );

	struct jack_info_s *new_jack = NULL;
	struct jack_info_s *traverse_jack = NULL;

	new_jack= ( struct jack_info_s * ) malloc( sizeof( struct jack_info_s ) );
	memset((void *) new_jack, 0x0,(sizeof(struct jack_info_s) ) );

	new_jack->timestamp = 01.07;
	new_jack->jack_type = alpha;
	new_jack->next_jack = NULL;

	if(jack->jack_state_per_device[0].head_jack == NULL)
	{
		jack->jack_state_per_device[0].head_jack = new_jack; 
	}

	else
	{
		traverse_jack = jack->head_jack;
		while(traverse_jack->next_jack==NULL) { traverse_jack = traverse_jack->next_jack; }
		traverse_jack->next_jack = new_jack;
	}

	cout<<jack->jack_state_per_device[0].head_jack->timestamp<<endl;
	#warning "add some code here"
}