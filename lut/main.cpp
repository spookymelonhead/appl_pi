#include <iostream>
#include "stdint.h"

using namespace std;


typedef enum
{
  INTRUSION_TYPE_PRIORITY_ZERO     = 0U,
  INTRUSION_TYPE_PRIORITY_ONE,
  INTRUSION_TYPE_PRIORITY_TWO,
  INTRUSION_TYPE_PRIORITY_THREE,
  INTRUSION_TYPE_PRIORITY_FOUR,
  INTRUSION_TYPE_PRIORITY_FIVE,
  INTRUSION_TYPE_PRIORITY_SIX,
  INTRUSION_TYPE_FALSE_ALARM,
  INTRUSION_TYPE_NO_INTRUSION,

  INTRUSION_TYPE_INVALID,
  INTRUSION_NUM_OF_TYPES            = INTRUSION_TYPE_INVALID

}kavach_rx_intrusion_type_e;


typedef struct
{
  uint8_t ir_intrusion_flags;
  kavach_rx_intrusion_type_e intrusion_type;

}intrusion_type_lut_s;


typedef struct
{
  intrusion_type_lut_s ir_intrusion_type[16];

}kavach_rx_intrsusion_type_lut_s;


kavach_rx_intrsusion_type_lut_s kavach_rx_intrusion_type_lut =
{
	{
		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

		{
			0b0000, INTRUSION_TYPE_INVALID
		},

	}
};

int main()
{

	return 0;
}