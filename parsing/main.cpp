#include <iostream>
#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "stdlib.h"

using namespace std;


typedef uint8_t BYTE;

#define CRONET_HEADER_SIZE (3)

#define CRONET_PACKET_FRAME_SIZE (13)


#define ZIGBEE_FRAME_DATA_SIZE (5)


#define ZIGBEE_API_FRAME_LENGTH_SIZE (2)

#define ZIGBEE_API_FRAME_START_DELIMITER (0x7E)

#define ZIGBEE_API_FRAME_SIZE (3)


#define PACKET_FRAME_SIZE ( ZIGBEE_API_FRAME_SIZE + ZIGBEE_FRAME_DATA_SIZE + CRONET_PACKET_FRAME_SIZE )  

typedef uint8_t uint8;

typedef struct
{
	union
	{
		BYTE packet_frame[PACKET_FRAME_SIZE];

		struct
		{
			union
			{
				BYTE api_frame[ZIGBEE_API_FRAME_SIZE];

				struct
				{
					BYTE api_start_delimiter;

					union
					{
						uint16_t length;

						struct
						{
							BYTE length_msb;
							BYTE length_lsb;

						}length_s;

					}length_u;

				}zigbee_api_frame_s;

			}zigbee_api_frame_u;


			union
			{
				BYTE zigbee_frame_data[ZIGBEE_FRAME_DATA_SIZE];

				struct
				{
					BYTE api_identifier;
					BYTE api_frame_id;

					union
					{
						uint16_t destination_address;

						struct
						{
							BYTE destination_address_msb;
							BYTE destination_address_lsb;

						}destination_address_s;

					}destination_address_u;

					BYTE option_byte;

				}zigbee_frame_data_s;

			}zigbee_frame_data_u;


			union
			{
				BYTE cronet_packet_frame[CRONET_PACKET_FRAME_SIZE];

				struct
				{
					BYTE cronet_header[CRONET_HEADER_SIZE];
					BYTE protocol_version;

					union
					{
						uint16_t packet_length;

						struct
						{
							BYTE packet_length_lsb;
							BYTE packet_length_msb;

						}packet_length_s;

					}packet_length_u;

					union
					{
						uint16_t source_address;

						struct
						{
							BYTE source_address_lsb;
							BYTE source_address_msb;

						}source_address_s;

					}source_address_u;

					union
					{
						uint16_t destination_address;

						struct
						{
							BYTE destination_address_lsb;
							BYTE destination_address_msb;

						}destination_address_s;

					}destination_address_u;

					union
					{
						uint16_t packet_type;

						struct
						{
							BYTE packet_type_lsb;
							BYTE packet_type_msb;

						}packet_type_s;

					}packet_type_u;

					BYTE sync_command;

				}cronet_frame_s;

			}cronet_frame_u;

		}packet_frame_s;

	}packet_frame_u;

}zigbee_packet_frame_s;


int main(int argc, char const *argv[])
{
	zigbee_packet_frame_s zigbee_packet_frame;

	BYTE received_packet[28] = {  		0x7E, 0x00, 0x17, 									\ 
																		0x01, 0x01, 												\ 
																		0x00, 0x02, 												\
																		0x00, 															\
																		0xFF, 0xFF,	0xFD, 									\ 
																		0x01,														    \
																		0x0D, 0x00, 												\
																		0x01, 0x00, 												\ 
																		0x02, 0x00, 												\
																		0xC9, 0x00, 												\
																		0x22, 															\
																 	  0x62, 0x61, 0x74, 0x6D, 0x61, 0x6E, \
																	  0xEF 	
																	};
 
	//BYTE jack [21]= { 0x7E, 0xFF, 0xFF, 0x01, 0x01, 0x02, 0x03, 0x04, 0xFF, 0xFF, 0xFD, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};

	memcpy(zigbee_packet_frame.packet_frame_u.packet_frame, received_packet, 21);

	zigbee_packet_frame_s jack;

	jack = zigbee_packet_frame;
	//zigbee_packet_frame = &received_packet[0];

	cout<<"\nParsed packet equals: ";
	//cout<<hex<<zigbee_packet_frame.packet_frame_u.packet_frame;

	for(uint32_t john = 0; john<21; john++)
	{
		cout<<"0x";
		cout<<hex<<(unsigned int)zigbee_packet_frame.packet_frame_u.packet_frame[john];
		cout<<" ";
	}



	cout<<endl;
	return 0;
}

/**



	cout<<"\nzigbee_packet_frame:";

	cout<<"\n\nzigbee api frame";
	cout<<"\napi start delimiter ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_api_frame_u.zigbee_api_frame_s.api_start_delimiter;
	cout<<"\nlength ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_api_frame_u.zigbee_api_frame_s.length_u.length;

	cout<<"\n\nzigbee frame data: ";
	cout<<"\nZigbee api identifier"<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_frame_data_u.zigbee_frame_data_s.api_identifier;
	cout<<"\nZigbee api frame id"<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_frame_data_u.zigbee_frame_data_s.api_frame_id;
	cout<<"\nZigbee destination"<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_frame_data_u.zigbee_frame_data_s.destination_address_u.destination_address;

	cout<<"\n\ncronet packet: ";
	cout<<"\ncronet header ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.cronet_header[0]; cout<<" ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.cronet_header[1]; cout<<" ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.cronet_header[2]; cout<<" ";
	cout<<"\nProtocol version ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.protocol_version;
	cout<<"\nPacket length ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.packet_length_u.packet_length;
	cout<<"\nSource address ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.source_address_u.source_address;
	cout<<"\nDestination address ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.destination_address_u.destination_address;
	cout<<"\nPacket type ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.packet_type_u.packet_type;
	cout<<"\nSync command ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.cronet_frame_u.cronet_frame_s.sync_command;

cout<<"\nlength ";
	cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_api_frame_u.zigbee_api_frame_s.length_u.length_s.length_msb;
cout<<"  ";
cout<<zigbee_packet_frame.packet_frame_u.packet_frame_s.zigbee_api_frame_u.zigbee_api_frame_s.length_u.length_s.length_lsb;	





[H1][L1][L2][P1][P2][D1][D2][D3][CRC]

0xFF 0xAB 0xCD 0xAB 0xCD 0xFA  

#define max_data_num_bytes (3)



packet_s * packet_ptr;



length_data = 0xABCD


400
L1 = 400 & 0xFF;
L2 = (400>>8) & 0xFF;

L = 0xABCD
L1 = 0xAB
L2 = 0xCD


typedef struct
{
	union
	{
		BYTE data_bytes[5 + max_data_num_bytes];

		struct
		{
			union
			{
				uint16 length_data;

				struct
				{
					BYTE length_lsb;
					BYTE length_msb;

				}length_data_s;
			}length_u;

			union
			{
				uint16 p_data;

				struct
				{
					BYTE p_lsb;
					BYTE p_msb;

				}p_data_s;
			}p_u;

			BYTE data_bytes[max_data_num_bytes];			 
		}

	}data_bytes_u;

}packet_s;

**/