#include "sleepfairy.h"

SF_sm_s SF_sm;
SF_config_s SF_config = 
{
	{
		{
			THINGSTEL_MPU6050_MD,
			0b1111,
			&initAccelerometer,
			&MPU6050_reset,
			&MPU6050_PutToSleep,
			&MPU6050_WakeFromSleep,
		},

		{
			THINGSTEL_MPU6050_DMP,
			0b1111,
			&MPU6050_DMP6_init,
			&MPU6050_reset,
			&MPU6050_PutToSleep,
			&MPU6050_WakeFromSleep,			
		},

		{
			THINGSTEL_BMP280,
			0b1111,
			&bmp280_init,

		},

		{
			THINGSTEL_GPS_MTK3339,
			0b1111;
		},

		{
			THINGSTEL_TEMPT6000,
			0b0011;

		},

		{
			THINGSTEL_HDC1080,
			0b0011;

		},

		{
			THINGSTEL_WISOL_SIGFOX,
			0b0000;
		},

		{
			THINGSTEL_STM32L4_UART1,
			0b0011;

		},

		{
			THINGSTEL_STM32L4_UART3,
			0b0011;
		},

		{
			THINGSTEL_STM32L4_UART2,
			0b0011;
		},

		{
			THINGSTEL_STM32L4_ADC,
			0b0011;
		},

		{
			THINGSTEL_STM32L4_I2C,
			0b0011;			
		},

		{
			THINGSTEL_STM32L4_DMA,
			0b0011;
		},

		{
			THINGSTEL_STM32L4_LPTIM,
			0b0011;
		},

		{
			THINGSTEL_STM32L4_CLOCK,
			0b0011;
		},

		{
			THINGSTEL_STM32L4,
			0b0011;
		},

	},
};

/* SF Init with Config in SF.c*/
common_return_e SF_Init()
{
	common_return_e status = COMMON_RETURN_SUCCESS;
	uint32_t counter = 0U;

	if(SF_STATE_INITIALIZED == SF_sm.state)
	{
		#ifdef DEBUG_EN
		INFO_MSF("SleepFairy is Already Initialized");
		#endif
		return;
	}

	if(COMMON_RETURN_SUCCESS == status)
	{
		for(counter = 0U; counter < SF_NUM_DEVICES; counter++)
		{

		}
	}

	return (status);
}

/* SF init with alternate config */
common_return_e SF_Init_AlternateConfig(SF_config_s * SF_config)
{

}

/* Inits all non-initialized devices in the order */
common_return_e SF_Init_Devices()
{

}

/* De-Inits all initialized device in the order */
common_return_e SF_De_Init_Devices()
{

}

/* Inits One device */
common_return_e SF_Init_Dev(ThingsTel_e device)
{

}

/* De-Inits One Device */
common_return_e SF_De_Init_Dev(ThingsTel_e device)
{

}

/* Sleep One Device */
common_return_e SF_De_Sleep_Dev(ThingsTel_e device)
{

}
/* Wake one Device */
common_return_e SF_De_Wake_Dev(ThingsTel_e device)
{

}

/* END OF FILE */