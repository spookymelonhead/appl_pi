#ifndef _SLEEP_FAIRY_
#define _SLEEP_FAIRY_

#include "main.h"
#include "debug.h"


/* CONFIG MACROS */
#define SF_NUM_DEVICES THINGSTEL_NUM


/* Function pointer definition for Device Init */
typedef void (*SF_device_init_fp_t) (void);

/* Function pointer definition for Device De-Init */
typedef void (*SF_device_de_init_fp_t) (void);

/* Function pointer definition to put Device to sleep */
typedef void (*SF_device_sleep_fp_t) (void);

/* Function pointer definition to wake Device from sleep */
typedef void (*SF_device_wake_fp_t) (void);

/* SF Device States */
typedef enum
{
	SF_DEVICE_STATE_DANGLING 			= 0U,

	SF_DEVICE_STATE_INITIALIZED,
	SF_DEVICE_STATE_AWAKE 				= SF_DEVICE_STATE_INITIALIZED,
	SF_DEVICE_STATE_PUT_TO_SLEEP,
	SF_DEVICE_STATE_DE_INITIALIZED,
	
	SF_DEVICE_STATE_FAILURE,
	SF_DEVICE_STATE_SKIP,

	SF_DEVICE_STATE_INVALID
	SF_DEVICE_STATE_NUM 					= SF_DEVICE_STATE_INVALID
	
}SF_device_state_e;

/* Sleep Fairy States */
typedef enum
{
	SF_STATE_DANGLING 			= 0U,
	SF_STATE_INITIALIZED,

	SF_STATE_INVALID,
	SF_STATE_NUM 						= SF_STATE_INVALID
	
}SF_state_e;

typedef struct
{
	union
	{
		uint8_t flags;
		struct
		{
			/* Below four flags are for config whether function call is available for the functionality */
			uint8_t init 					:1;
			uint8_t de_init 			:1;
			uint8_t sleep 				:1;
			uint8_t awake 				:1;

			uint8_t unused1 			:1;
			uint8_t unused2  			:1;
			uint8_t unused3  			:1;
			uint8_t unused4 			:1;
		}flags_s;
	}flags_u;
}SF_device_config_flag_t;

typedef struct
{
	/* Device Alias */
	ThingsTel_e device;

	/* SF Device Config Flags */
	SF_device_config_flag_t flags;

	/* Init Function pointer */
	SF_device_init_fp_t init_fp;

	/* De-Init Function pointer */
	SF_device_de_init_fp_t de_init_fp;

	/* Sleep Function pointer */
	SF_device_sleep_fp_t sleep_fp;

	/* Wake Function pointer */
	SF_device_wake_fp_t wake_fp;

}SF_config_per_device_s;

typedef struct
{
	SF_config_per_device_s device_config[SF_NUM_DEVICES];
	
}SF_config_s;

/* SF Device SM */
typedef struct
{
	/* State */
	SF_device_state_e state;

	/* Init Function pointer */
	SF_device_init_fp_t init_fp;

	/* De-Init Function pointer */
	SF_device_de_init_fp_t de_init_fp;

	/* Sleep1 Function pointer */
	SF_device_sleep_fp_t sleep1_fp;

	/* Sleep Function pointer */
	SF_device_sleep_fp_t sleep_fp;	

	/* Wake Function pointer */
	SF_device_wake_fp_t wake_fp;

}SF_device_sm_s;

/* SLEEP FAIRY SM type definition*/
typedef struct
{
	/* State */
	SF_state_e state;

	/* THingsTel Devices */
	SF_device_sm_s device[SF_NUM_DEVICES];
	
}SF_sm_s;

extern SF_sm_s SF_sm;
extern SF_config_s SF_config;

/* SF Init with Config in SF.c*/
common_return_e SF_Init();
/* SF init with alternate config */
common_return_e SF_Init_AlternateConfig(SF_config_s * SF_config);
/* Inits all non-initialized devices in the order */
common_return_e SF_Init_Devices();
/* De-Inits all initialized device in the order */
common_return_e SF_De_Init_Devices();
/* Inits One device */
common_return_e SF_Init_Dev(ThingsTel_e device);
/* De-Inits One Device */
common_return_e SF_De_Init_Dev(ThingsTel_e device);
/* Sleep One Device */
common_return_e SF_De_Sleep_Dev(ThingsTel_e device);
/* Wake one Device */
common_return_e SF_De_Wake_Dev(ThingsTel_e device);

#endif /* _SLEEP_FAIRY_ */