#include <iostream>
#include <stdint.h>

#define CRONET_PACKET_HEADER_FIELD_LENGTH                 3
#define CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH       1
#define CRONET_PACKET_DATA_LENGTH_FIELD_LENGTH            2
#define CRONET_PACKET_SOURCE_ADDRESS_FIELD_LENGTH         2
#define CRONET_PACKET_DESTINATION_ADDRESS_FIELD_LENGTH    2
#define CRONET_PACKET_PACKET_TYPE_FIELD_LEGNTH            2
#define CRONET_PACKET_SYNC_COMMAND_FIELD_LENGTH           1
#define CRONET_PACKET_DATA_FIELD_LENGTH                   1

#define CRONET_PACKET_LENGTH                              ( CRONET_PACKET_HEADER_FIELD_LENGTH + \
                                                            CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH + \
                                                            CRONET_PACKET_DATA_LENGTH_FIELD_LENGTH + \
                                                            CRONET_PACKET_SOURCE_ADDRESS_FIELD_LENGTH + \
                                                            CRONET_PACKET_DESTINATION_ADDRESS_FIELD_LENGTH + \
                                                            CRONET_PACKET_PACKET_TYPE_FIELD_LEGNTH + \
                                                            CRONET_PACKET_SYNC_COMMAND_FIELD_LENGTH )

#define FRAME_DATA_FIELD_API_IDENTIFIER_FIELD_LEGNTH        1
#define FRAME_DATA_FIELD_API_FRAME_ID_FIELD_LEGNTH          1
#define FRAME_DATA_FIELD_DESTINATION_ADDRESS_FIELD_LENGTH   2
#define FRAME_DATA_FIELD_OPTION_BYTE                        1
#define FRAME_DATA_FIELD_PACKET_LENGTH                      ( FRAME_DATA_FIELD_API_IDENTIFIER_FIELD_LEGNTH + \
                                                              FRAME_DATA_FIELD_API_FRAME_ID_FIELD_LEGNTH + \
                                                              FRAME_DATA_FIELD_DESTINATION_ADDRESS_FIELD_LENGTH + \
                                                              FRAME_DATA_FIELD_OPTION_BYTE )

using namespace std;

/*==============================================================================
                     LOCAL DEFINITIONS AND TYPES : TYPEDEFS
==============================================================================*/

typedef uint8_t BYTE;

/*----------------------------------------------------------------------------*/
/*!@brief struct to define the parsing cronet packet */
typedef struct
{
  union
  {
    BYTE cronet_packet_frame[CRONET_PACKET_LENGTH];

    struct
    {
      BYTE cronet_header[CRONET_PACKET_HEADER_FIELD_LENGTH];
      BYTE cronet_version[CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH];

      union
      {
        BYTE tom[CRONET_PACKET_LENGTH - CRONET_PACKET_HEADER_FIELD_LENGTH - CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH];

        struct
        {
          BYTE cronet_data_length[CRONET_PACKET_DATA_LENGTH_FIELD_LENGTH];

          union
          {
            BYTE dic[CRONET_PACKET_LENGTH - CRONET_PACKET_HEADER_FIELD_LENGTH-CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH- \
                     CRONET_PACKET_DATA_LENGTH_FIELD_LENGTH];

            struct
            {
              BYTE cronet_source_address[CRONET_PACKET_SOURCE_ADDRESS_FIELD_LENGTH];
              BYTE cronet_destination_address[CRONET_PACKET_DESTINATION_ADDRESS_FIELD_LENGTH];
              
              union
              {
                BYTE harry[CRONET_PACKET_LENGTH - CRONET_PACKET_HEADER_FIELD_LENGTH-CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH- \
                           CRONET_PACKET_DATA_LENGTH_FIELD_LENGTH- CRONET_PACKET_SOURCE_ADDRESS_FIELD_LENGTH - \
                           CRONET_PACKET_DESTINATION_ADDRESS_FIELD_LENGTH];

                struct
                {
                  BYTE cronet_packet_type[CRONET_PACKET_PACKET_TYPE_FIELD_LEGNTH];

                  union
                  {
                    BYTE ram[CRONET_PACKET_LENGTH - CRONET_PACKET_HEADER_FIELD_LENGTH-CRONET_PACKET_PROTOCOL_VERSION_FIELD_LENGTH - \
                             CRONET_PACKET_DATA_LENGTH_FIELD_LENGTH- CRONET_PACKET_SOURCE_ADDRESS_FIELD_LENGTH -\
                             CRONET_PACKET_DESTINATION_ADDRESS_FIELD_LENGTH - CRONET_PACKET_PACKET_TYPE_FIELD_LEGNTH ];

                    struct
                    {
                      BYTE cronet_sync_commnad[CRONET_PACKET_SYNC_COMMAND_FIELD_LENGTH];

                    }sync_command_s;

                  }sync_command_u;

                }packet_type_s;

              }packet_type_u;

            }address_s;

          }address_u;          

        }data_length_s;

      }data_length_u;

    }header_version_s;

  }header_version_u;

}cronet_packet_s;

/*----------------------------------------------------------------------------*/
/*!@brief struct to define the parsing frame data field packet */
typedef struct
{
  union
  {
    BYTE frame_data_field_packet[FRAME_DATA_FIELD_PACKET_LENGTH];

    struct
    {
      BYTE frame_data_field_api_identifier[FRAME_DATA_FIELD_API_IDENTIFIER_FIELD_LEGNTH];

      union
      {
        BYTE tom[FRAME_DATA_FIELD_PACKET_LENGTH - FRAME_DATA_FIELD_API_IDENTIFIER_FIELD_LEGNTH];

        struct
        {
          BYTE frame_data_field_api_frame_id[FRAME_DATA_FIELD_API_FRAME_ID_FIELD_LEGNTH];

          union
          {
            BYTE dic[FRAME_DATA_FIELD_PACKET_LENGTH - FRAME_DATA_FIELD_API_IDENTIFIER_FIELD_LEGNTH - FRAME_DATA_FIELD_API_FRAME_ID_FIELD_LEGNTH];
          
            struct
            {
              BYTE frame_data_field_destination_address[FRAME_DATA_FIELD_DESTINATION_ADDRESS_FIELD_LENGTH];

              union
              {
                BYTE harry[FRAME_DATA_FIELD_PACKET_LENGTH - FRAME_DATA_FIELD_API_IDENTIFIER_FIELD_LEGNTH - FRAME_DATA_FIELD_API_FRAME_ID_FIELD_LEGNTH - \
                         FRAME_DATA_FIELD_DESTINATION_ADDRESS_FIELD_LENGTH];
                struct
                {
                  BYTE frame_data_field_option_byte[FRAME_DATA_FIELD_OPTION_BYTE];

                }option_byte_s;

              }option_byte_u;

            }destination_address_s;
          
          }destination_address_u;

        }api_frame_id_s;

      }api_frame_id_u;

    }api_identifier_s;

  }api_identifier_u;

}frame_data_field_s;

int main(int argc, char const *argv[])
{
  cronet_packet_s cronet_packet;
  frame_data_field_s frame_data_field;

  
  cout<<endl;  
  
  return 0;
}


void into_packet()
{
  cronet_packet.header_version_u.header_version_s.cronet_header[0] = 0xFF;
  cronet_packet.header_version_u.header_version_s.cronet_header[1] = 0xFF;
  cronet_packet.header_version_u.header_version_s.cronet_header[2] = 0xFD;
  cronet_packet.header_version_u.header_version_s.cronet_version[0] = 0x01;

  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.cronet_data_length[0] = 0x00;
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.cronet_data_length[1] = 0x08;
  
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.cronet_source_address[0] = 0xFF;
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.cronet_source_address[1] = 0xFF;
  
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.cronet_destination_address[0] = 0xFE;
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.cronet_destination_address[1] = 0xFE;
  
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.packet_type_u.packet_type_s.cronet_packet_type[0] = 0x01;
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.packet_type_u.packet_type_s.cronet_packet_type[1] = 0x01;
  
  cronet_packet.header_version_u.header_version_s.data_length_u.data_length_s.address_u.address_s.packet_type_u.packet_type_s.sync_command_u.sync_command_s.cronet_sync_commnad[0] = 0x22;

  cout<<endl<<"cronet packet_equals 0x";
  for(uint8_t i = 0; i< CRONET_PACKET_LENGTH; i++)
  {
    cout<<hex<<(int)cronet_packet.header_version_u.cronet_packet_frame[i]<<(" ");
  }
  cout<<endl;

  frame_data_field.api_identifier_u.api_identifier_s.frame_data_field_api_identifier[0] = 0x01;
  frame_data_field.api_identifier_u.api_identifier_s.api_frame_id_u.api_frame_id_s.frame_data_field_api_frame_id[0] = 0x01;

  frame_data_field.api_identifier_u.api_identifier_s.api_frame_id_u.api_frame_id_s.destination_address_u.destination_address_s.frame_data_field_destination_address[0] = 0xFE;
  frame_data_field.api_identifier_u.api_identifier_s.api_frame_id_u.api_frame_id_s.destination_address_u.destination_address_s.frame_data_field_destination_address[1] = 0xFE;

  frame_data_field.api_identifier_u.api_identifier_s.api_frame_id_u.api_frame_id_s.destination_address_u.destination_address_s.option_byte_u.option_byte_s.frame_data_field_option_byte[0] = 0x00;

  cout<<endl<<"cronet packet_equals 0x";
  for(uint8_t i = 0; i< FRAME_DATA_FIELD_PACKET_LENGTH; i++)
  {
    cout<<hex<<(int)frame_data_field.api_identifier_u.frame_data_field_packet[i]<<(" ");
  }
}