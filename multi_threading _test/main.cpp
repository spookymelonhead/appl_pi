#include <iostream>
#include "stdint.h"
#include "stdbool.h"

using namespace std;

int main(int argc, char ** argv)
{
	/* Code */
	if(argc < 2U)
	{
		cout<<"\nError, no argument passed, Abort";
	}

	else
	{
		cout<<(char *) argv[1];

	}
	cout<<endl;
	return 0;
}