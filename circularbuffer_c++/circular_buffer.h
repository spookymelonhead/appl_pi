/****************************************************************************
 *
 *   Copyright (C) 2015-2017. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file circularbuffer.h
 *
 * A flexible circular buffer class.
 * author: Hardik Madaan
 * alias: Brian doofus
 * email: hardik.mad@gmail.com
 *
 */

#pragma once

#include "stdint.h"
#include "stdbool.h"
#include <iostream>



namespace circular_buffer
{
	typedef struct
	{
		size_t number_of_items;
		size_t item_size;
		
	}circular_buffer_config_s;

	class circular_buffer
	{
	public:
		// constructor for the class
		circular_buffer(uint32_t number_of_items, size_t item_size);
		~circular_buffer();

		/**
		 * @breif:  function to put an item into the buffer, if bufferis not full.
		 * @param:  item to put.
		 * @return: boolean status, True if item was put, false if buffer is full.
		**/
		bool put(uint32_t val);

		/**
		 * @breif:  function to force put an item into the buffer, if buffer is full.
		 * @param:  item to put.
		 * @return: boolean status, True if item was put and an item was discared.
		**/
		bool force_put(uint32_t val);

		/**
		 * @breif:  function to get an item from the buffer.
		 * @param:  item to get.
		 * @return: boolean status, True if item was put, false if not.
		**/
		bool get(uint32_t val);

		/**
		 * @breif:  function to get how many space left.
		 * @param:  void.
		 * @return: uint32 space left before buffer is full.
		**/
		uint32_t space_left(void);

		/**
		 * @breif:  function to get how many items in buffer.
		 * @param:  void.
		 * @return: uint32 number of items in buffer.
		**/
		uint32_t count_items(void);

		/**
		 * @breif:  function to know if buffer is empty.
		 * @param:  void.
		 * @return: booelan, True is empty.
		**/
		bool is_empty(void);

		/**
		 * @breif:  function to know if buffer is full.
		 * @param:  void.
		 * @return: booelan, True is full.
		**/
		bool is_full(void);

		/**
		 * @breif:  function to flush buffer.
		 * @param:  void.
		 * @return: void.
		**/
		void flush(void);

		/**
		 * @breif:   function to resize buffer.
		 * @param:   void.
		 * @comment: this is unsafe.
		 * @return:  uint32, new size of buffer and zero if failed.
		**/
		uint32_t resize(void);

		/**
		 * @breif:  function to print debug info.
		 * @param:  void.
		 * @return: void.
		**/
		void print_debug(void);

	protected:
	private:
		circular_buffer_config_s config;
		uint32_t num_of_items;
		uint32_t * _buffer;
		volatile uint32_t _head, _tail;
	};

}// namespace circular_buffer

