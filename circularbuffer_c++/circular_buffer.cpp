#include "circular_buffer.h"
#include "stdlib.h"
#include "string.h"

namespace circular_buffer
{
	circular_buffer :: circular_buffer(uint32_t number_of_items, size_t item_size)
	{
		cout<<"\nConstructor calling";
		// number of item currently in buffer
		number_of_items = 0U;
		// max nmber f items in buffer
		config.number_of_items = number_of_items;
		// item size
		config.item_size = item_size;
	}

	circular_buffer :: ~circular_buffer()
	{
		if(NULL != _buffer)
		{
			delete[] _buffer;
		}
	}

	bool circular_buffer :: is_empty(void)
	{
		return (_head == _tail);
	}

	bool circular_buffer :: is_full(void)
	{
		return (_head == _tail);
	}
}// namespace circular_buffer