#include <iostream>

using namespace std;

typedef struct
{
	union
	{
		unsigned int led_flag;

		struct
		{
			unsigned int led_zero :1;
			unsigned int led_one :1;
			unsigned int led_two :1;
			unsigned int led :1;

		}flag_s;

	}flag_u;
	
}led_enable_s;


int main(int argc, char const *argv[])
{
	led_enable_s led_enable_reg;
	led_enable_reg.flag_u.led_flag = 0U;

	cout<<"Hey man"<<endl;

	led_enable_reg.flag_u.flag_s.led_zero = 1;
	cout<<led_enable_reg.flag_u.led_flag<<endl;

	return 0;
}
