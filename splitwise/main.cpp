#include <iostream>
#include "stdlib.h"
#include <stdint.h>
#include "stdio.h"

using namespace std;

struct tim
{
	char *name;
	uint32_t id;
	uint32_t *giveto, *takefrom;
	uint32_t *givenamt;
	int *takenamt;
};

int main(int argc, char const *argv[])
{
	int n = 0U;
	struct tim *tim_ptr;
	cout<<endl<<"Enter no of peopel ";
	cin>>n;

	tim_ptr = ( struct tim * )malloc( sizeof( struct tim) * n );

	for(uint32_t i=0; i<n; i++)
	{
		tim_ptr->giveto   = (uint32_t *) malloc( sizeof(uint32_t) * n );
		tim_ptr->takefrom = (uint32_t *) malloc( sizeof(uint32_t) * n );
		tim_ptr->givenamt = (uint32_t *) malloc( sizeof(uint32_t) * n );
		tim_ptr->takenamt = (int *)      malloc( sizeof(int) * n );
		
		tim_ptr->id=n;

		cout<<"\nEnter details for subject "<<(i);
		
		//cout<<"Enter name: ";
		//cin.getline(*tim_ptr->name,20);

		cout<<"\nGiven to: ";
		cin>>*tim_ptr->giveto;

		cout<<"\nGiven amount: ";
		cin>>*tim_ptr->givenamt;
		
		cout<<"\nTaken from: ";
		cin>>*tim_ptr->takefrom;

		cout<<"\nTaken amount: ";
		cin>>*tim_ptr->takenamt;

		*tim_ptr->takenamt = *tim_ptr->takenamt * -1;

		if(i+1 == n)
			tim_ptr = tim_ptr - n;

		tim_ptr++;
	}

	for(uint32_t i=0; i<n; i++)
	{
		cout<<"\nDetails for subject "<<i;

		cout<<"\nGiven to: ";
		cout<<*tim_ptr->giveto;

		cout<<"\nGiven amount: ";
		cout<<*tim_ptr->givenamt;
		
		cout<<"\nTaken from: ";
		cout<<*tim_ptr->takefrom;

		cout<<"\nTaken amount: ";
		cout<<*tim_ptr->takenamt;

		cout<<endl;		
	}	


	return 0;
}