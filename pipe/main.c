#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/types.h"
#include "string.h"

char * cstring = "child says hi to parent\n";
char * pstring = "parent nods\n";

int main()
{
	int fd[2];
	int err_status = 0;
	pid_t cpid;
	int a = 7, b = 62;

	err_status = pipe(fd);
	if(err_status < 0)
	{
		printf("pipe failed\n");
		exit(0);
	}
	cpid = fork();
	if(cpid < 0)
	{
		printf("fork failed\n");
		exit(0);
	}
	else if(cpid == 0)
	{
		close(fd[0]);
		while(1)
		{
			write(fd[1],cstring, strlen(cstring)+1);
			sleep(2);
		}
		exit(0);
	}
	else
	{
		char rstring[20];
		int num = 0;
		close(fd[1]);
		while(1)
		{
			num = read(fd[0], rstring, strlen(cstring));
			if(num)
			{
				printf("%s", rstring);
				printf("%s", pstring);
			}
			else
			{
				printf("str NULLrx\n");
			}
			sleep(1);
		}
	}
	return (0);
}