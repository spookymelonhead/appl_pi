#include <iostream>
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

using namespace std;

#define ROWS (2U)
#define COLS (2U)

uint16_t **ptr_to_ptr_16 = NULL;
uint32_t rcounter, ccounter;

int main(int argc, char ** argv)
{
	cout<<"HERE1";

	*(ptr_to_ptr_16) = (uint16_t *) calloc(ROWS, sizeof(uint16_t));

	ptr_to_ptr_16 = (uint16_t **) calloc(COLS, sizeof(uint16_t));

	memset(ptr_to_ptr_16, 0xFFFF, ROWS);

	memset(*(ptr_to_ptr_16), 0xFFFF, COLS);

  cout<<hex<<(uint16_t) ptr_to_ptr_16[0][1];

	for(rcounter = 0U; rcounter < ROWS; rcounter += 1U)
	{
			for(ccounter = 0U; ccounter < COLS; ccounter += 1U)
			{
				cout<<"\nElement Row " << rcounter << " and Column " << ccounter <<" equals "<<ptr_to_ptr_16[rcounter][ccounter];
			}		
	}

	return 0U;
}