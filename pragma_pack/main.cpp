#include <stdio.h>
#include <iostream>
#include "stdint.h"
#include "stdbool.h"

using namespace std;

#define pragma_val (1U)

#pragma pack(push,1U)
typedef struct
{
	uint8_t al;
	uint32_t bl;
	uint16_t cl;

}tuna_t;

int main(int argc, char** argv)
{
	uint8_t * al_ptr = NULL;
	uint32_t * bl_ptr = NULL;
	uint16_t * cl_ptr = NULL;

	tuna_t tuna = {0xFF, 0xFFFFFFF, 0xFFFF};

	al_ptr = & tuna.al;
	bl_ptr = & tuna.bl;
	cl_ptr = & tuna.cl;

	cout<<"\nPragma Val: "<<pragma_val;
	cout<<"\nSize of tuna is :"<< sizeof(tuna);
	cout<<"\nal_ptr points to "<<hex<<al_ptr;
	cout<<"\nbl_ptr points to "<<hex<<bl_ptr;
	cout<<"\ncl_ptr points to "<<hex<<cl_ptr;
	cout<<endl;


	cout<<"\nal_ptr points to "<<hex<<(uint8_t)*al_ptr;
	cout<<"\nbl_ptr points to "<<hex<<(uint32_t)*bl_ptr;
	cout<<"\ncl_ptr points to "<<hex<<(uint16_t)*cl_ptr;

	return 0;
}
