#include <iostream>
#include <math.h>

using namespace std;

/* class bicycle start */
class bicycle{
    public:
   
    void move_forward(int step=2);
    virtual void steer_at_angle(int angle);
    bicycle(int posX=0, int posY=0);
    ~bicycle();
    void where_am_i();
   
    private:
    int posX;
    int posY;
    double steer_angle;
    const int wheels;
    int health;
   
    protected:
    /* nothing here */
};
bicycle:: bicycle(int posX, int posY):wheels(2){
    this->posX = posX;
    this->posY= posY;
    this->health = 10;
    this->steer_angle = 0;
    cout<<"Your bicycle is ready!"<<endl;
    return;
}
bicycle:: ~bicycle(){
    return;
}
void bicycle:: move_forward(int step){
    this->posX += step * cos(this->steer_angle);
    this->posY += step * sin(this->steer_angle);
    return;
}
void bicycle:: steer_at_angle(int angle){
    cout<<"bicycle"<<endl;
    this->steer_angle = angle * (M_PI / 180);
}
void bicycle:: where_am_i(){
    cout<<"x: "<<this->posX<<" y: "<<this->posY<<endl;    
}
/* class bicycle end */

/* class geared_bicycle starts */
class geared_bicycle: public bicycle{
    public:
   
    geared_bicycle(int posX, int posY);
    void gear_up();
    void gear_down();
    void move_forward();
    void steer_at_angle(int angle);
   
    private:
    const int num_gears;
    int gear;
   
    protected:
    /* nothing here */
};
geared_bicycle::geared_bicycle(int posX, int posY):num_gears(5), bicycle(posX,posY){
    this->gear = 0;
    cout<<"Your geared bicycle is ready!!"<<endl;
}
void geared_bicycle::gear_up(){
    if(num_gears == gear) return;
    this->gear += 1;
}
void geared_bicycle::gear_down(){
    if(0 == this->gear) return;
    this->gear -= 1;
}
void geared_bicycle::move_forward(){
    this->bicycle::move_forward(this->gear);
    return;  
}
void geared_bicycle:: steer_at_angle(int angle){
    cout<<"geared bicycle"<<endl;
    cout<<"calling base class steer at angle from derived class func"<<endl;
    this->bicycle::steer_at_angle((angle/2) * (M_PI / 180));
}
/* class geared_bicycle ends */
int main()
{
    cout<<"Hello World"<<endl;
    /* Bicycle*/
    bicycle * myBike = new bicycle(0,0);
    myBike->steer_at_angle(0);
    for(int i=0; i<5; i++){
        myBike->move_forward();
        myBike->where_am_i();        
    }
    /* Geared bicycle */
    geared_bicycle * myGearedBike = new geared_bicycle(5,5);
    myGearedBike->steer_at_angle(0);
    myGearedBike->gear_up();
    myGearedBike->gear_up();
    for(int i=0; i<5; i++){
        myGearedBike->move_forward();
        myGearedBike->where_am_i();        
    }
    myGearedBike->gear_down();
    myGearedBike->gear_down();
    /* checking runtime polymorphism with virtual function steer_at_angle */
    geared_bicycle * myGearedBike2 = new geared_bicycle(5,5);
    bicycle * myBike2 = myGearedBike2;
    myBike2->steer_at_angle(20);
   
    return 0;
}
