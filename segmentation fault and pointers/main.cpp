#include <iostream>
#include <stdlib.h>
#include "string.h"

using namespace std;


int main()
{
	int toothferry = 12;
	int *iptr = &toothferry;

	cout<<"\niptr equals "<<*iptr<<endl;
	cout<<"\ntoothferry equals "<<toothferry<<endl;
	cout<<"\ntoothferry address equals "<<&toothferry<<endl;

	return 0;
}