#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>


void array_left_shifter(int * arr, int num_ele, int d );
void print_an_array(int* arr, int size);

int main()
{
    // number of elements in the array
    int num_ele; 
    // number of leftshifts
    int d;
    // array
    int *arr = NULL;
    
    scanf("%d %d", &num_ele, &d);
    
    arr = (int *) calloc(num_ele, sizeof(int));
    
    for(int count = 0; count < num_ele; count++)
    {
       scanf("%d",&arr[count]);
    }
    
    array_left_shifter( arr, num_ele, d );    
    print_an_array(arr, num_ele);
    
    return 0;
}

void array_left_shifter(int * arr, int num_ele, int d )
{
    int count = 0;
    int shift_count = 0;
    int first_element = 0;
    int * arr_cpy = NULL;
    
    if(d > num_ele)
    {
        d = d % num_ele;
    }
    
    arr_cpy = (int *)calloc(num_ele, sizeof(int));
    
    for(count = 0; count < num_ele; count ++)
    {
        arr_cpy[count] = arr[count];
    }
    
    
    for(count = 0; count < num_ele; count += 1)
    {
        if(count - d < 0)
        {
            arr[count - d + num_ele] = arr_cpy[count];        
        }
        else
        {
            arr[count - d] = arr_cpy[count];
        }
        //print_an_array(arr, num_ele);
    }
    
}

void print_an_array(int* arr, int size)
{
    int count = 0;
    for(int count = 0; count < size; count++)
    {
       printf("%d ", arr[count]);
    }
    printf("\n");
}