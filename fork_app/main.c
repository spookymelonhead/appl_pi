#include "stdio.h"
#include "stdint.h"
#include "stdbool.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "stdlib.h"
#include "string.h"

#define USE_VFORK
// #define USE_FORK

#if defined USE_VFORK
  #if defined USE_FORK
    #error "define one macro at a time either USE_FORK or USE_VFORK"
  #endif
#endif

#if defined USE_VFORK
  #define WITCH_FORK() vfork()
#elif defined USE_FORK
  #define WITCH_FORK() fork()
#else
  #error "dont know which fork to use"
#endif

#define MAX_CP    (10U)
typedef pid_t (*cp_fptr_t)(void);

pid_t printi()
{
  printf("i\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t printm()
{
  sleep(2);
  printf("m\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t printb()
{
  printf("b\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t printa()
{
  printf("a\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t printt()
{
  printf("t\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t printn()
{
  printf("n\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t printnl()
{
  printf("\n\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
pid_t phantom()
{
  printf("phantom\n");
  printf("\t pid:%d\n", getpid());
  return(getpid());
}
typedef struct
{
  cp_fptr_t cp_fptr[MAX_CP];

}imbatman_config_ts;
imbatman_config_ts imbatman_config = 
{
  {
    &printi,
    &printm,
    &printb,
    &printa,
    &printt,
    &printm,
    &printa,
    &printn,
    &printnl,
    &phantom,
  },
};

/* State machine stuff */
typedef struct
{
  pid_t cp_pid[MAX_CP];
  cp_fptr_t cp_fptr[MAX_CP]; 
  uint32_t cp_counter;
}imbatman_sm_ts;

/* Global Var */
imbatman_sm_ts imbatman_sm;

int init_sm(imbatman_sm_ts * sm);

pid_t task();

int main()
{
  int status = 0;
  pid_t temp_pid;
  printf("Program starts..!!\n");
  printf("PID %d\n", getpid());
  printf("PID %d PPID %d\n",getpid(), getppid());
  printf("bamboozled %d\n", getpid());

  init_sm(&imbatman_sm);
  temp_pid = WITCH_FORK();
  temp_pid = WITCH_FORK();
  temp_pid = WITCH_FORK();
  if(temp_pid < 0)
  {
    printf("Fork failed\n");
    status = -1;
    return (status);
  }
  else if(temp_pid > 0)
  {
    task();
  }
  else
  {
    //Child processes who don't call fork.
    task();
  }

  printf("\n");
  exit (0);
}

int init_sm(imbatman_sm_ts * sm)
{
  //init cp_pid to zero
  memset((void *)sm->cp_pid, 0x0, sizeof(pid_t) * 10 );
  sm->cp_counter = 0U;
  for(uint32_t counter = 0U; counter < MAX_CP; counter++)
  {
    sm->cp_fptr[counter] = imbatman_config.cp_fptr[counter];
  }
  printf("init sm success\n");
  return (0);
}

pid_t task()
{
  imbatman_sm.cp_pid[imbatman_sm.cp_counter] = getpid();
  (*(imbatman_sm.cp_fptr[imbatman_sm.cp_counter]))();
  if(MAX_CP > imbatman_sm.cp_counter + 1U)
  {
    imbatman_sm.cp_counter++;
  }
  else
  {
    printf("cp_counter max value reached\n");
    exit(1);
  }
}


/*
PARENT:     Program starts..!!
PARENT:     JACK:Process ID 24984
PARENT:     JACK:Process ID 24984 ParentProcess ID 24536
PARENT:     bhosadpappu
CHILD:      This is the 24987 Process doing things..!!
CHILD:      Process ID 24987 and its ParentProcess ID 24984
UNKNOWN:		bhosadpappu
PARENT:			This is the 24984 Process doing things..!!
PARENT:			Process ID 24984 and its ParentProcess ID 24536
*/

/*
*  The child has its own unique process ID, and this PID does not
          match the ID of any existing process group (setpgid(2)) or
          session.

       *  The child's parent process ID is the same as the parent's process
          ID.

       *  The child does not inherit its parent's memory locks (mlock(2),
          mlockall(2)).

       *  Process resource utilizations (getrusage(2)) and CPU time counters
          (times(2)) are reset to zero in the child.

       *  The child's set of pending signals is initially empty
          (sigpending(2)).

       *  The child does not inherit semaphore adjustments from its parent
          (semop(2)).

       *  The child does not inherit process-associated record locks from
          its parent (fcntl(2)).  (On the other hand, it does inherit
          fcntl(2) open file description locks and flock(2) locks from its
          parent.)

       *  The child does not inherit timers from its parent (setitimer(2),
          alarm(2), timer_create(2)).

       *  The child does not inherit outstanding asynchronous I/O operations
          from its parent (aio_read(3), aio_write(3)), nor does it inherit
          any asynchronous I/O contexts from its parent (see io_setup(2)).

       The process attributes in the preceding list are all specified in
       POSIX.1.  The parent and child also differ with respect to the
       following Linux-specific process attributes:

       *  The child does not inherit directory change notifications
          (dnotify) from its parent (see the description of F_NOTIFY in
          fcntl(2)).

       *  The prctl(2) PR_SET_PDEATHSIG setting is reset so that the child
          does not receive a signal when its parent terminates.

       *  The default timer slack value is set to the parent's current timer
          slack value.  See the description of PR_SET_TIMERSLACK in
          prctl(2).

       *  Memory mappings that have been marked with the madvise(2)
          MADV_DONTFORK flag are not inherited across a fork().

       *  Memory in address ranges that have been marked with the madvise(2)
          MADV_WIPEONFORK flag is zeroed in the child after a fork().  (The
          MADV_WIPEONFORK setting remains in place for those address ranges
          in the child.)

       *  The termination signal of the child is always SIGCHLD (see
          clone(2)).

       *  The port access permission bits set by ioperm(2) are not inherited
          by the child; the child must turn on any bits that it requires
          using ioperm(2).

       Note the following further points:

       *  The child process is created with a single thread—the one that
          called fork().  The entire virtual address space of the parent is
          replicated in the child, including the states of mutexes,
          condition variables, and other pthreads objects; the use of
          pthread_atfork(3) may be helpful for dealing with problems that
          this can cause.

       *  After a fork() in a multithreaded program, the child can safely
          call only async-signal-safe functions (see signal-safety(7)) until
          such time as it calls execve(2).

       *  The child inherits copies of the parent's set of open file
          descriptors.  Each file descriptor in the child refers to the same
          open file description (see open(2)) as the corresponding file
          descriptor in the parent.  This means that the two file
          descriptors share open file status flags, file offset, and signal-
          driven I/O attributes (see the description of F_SETOWN and
          F_SETSIG in fcntl(2)).

       *  The child inherits copies of the parent's set of open message
          queue descriptors (see mq_overview(7)).  Each file descriptor in
          the child refers to the same open message queue description as the
          corresponding file descriptor in the parent.  This means that the
          two file descriptors share the same flags (mq_flags).

       *  The child inherits copies of the parent's set of open directory
          streams (see opendir(3)).  POSIX.1 says that the corresponding
          directory streams in the parent and child may share the directory
          stream positioning; on Linux/glibc they do not.
RETURN VALUE         top
       On success, the PID of the child process is returned in the parent,
       and 0 is returned in the child.  On failure, -1 is returned in the
       parent, no child process is created, and errno is set appropriately.

*/
