/*
http://stackoverflow.com/questions/1371460/state-machines-tutorials/1371654#1371654
State machines are very simple in C if you use function pointers.
Basically you need 2 arrays - one for state function pointers and one for state 
transition rules. Every state function returns the code, you lookup state 
transition table by state and return code to find the next state and then 
just execute it.
*/

#include <iostream>

#define EXIT_STATE end
#define ENTRY_STATE entry

typedef enum
{   entry,
    foo,
    bar,
    end
}state_codes;

typedef enum
{
    ok,
    fail,
    repeat
}ret_codes;

struct transition
{
    state_codes src_state;
    ret_codes   ret_code;
    state_codes dst_state;
};

/* transitions from end state aren't needed */
struct transition state_transitions[] =
{
    {entry, ok,     foo},
    {entry, fail,   end},
    {foo,   ok,     bar},
    {foo,   fail,   end},
    {foo,   repeat, foo},
    {bar,   ok,     end},
    {bar,   fail,   end},
    {bar,   repeat, foo}
};

int entry_state(void);
int foo_state(void);
int bar_state(void);
int exit_state(void);

int (* state_fptr[4])(void) = { entry_state, foo_state, bar_state, exit_state};

int main(int argc, char **argv)
{
    state_codes cur_state = ENTRY_STATE;
    ret_codes rc;
    
    int (* state_fun)(void);

    for (;;)
    {
        state_fun = state_fptr[cur_state];
        rc = state_fun();
        if (EXIT_STATE == cur_state)
            break;
        cur_state = lookup_transitions(cur_state, rc);
    }

    return EXIT_SUCCESS;
}
