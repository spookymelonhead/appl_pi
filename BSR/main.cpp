#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
	float clock_scaler;
	unsigned int prescaler,fractional_prescaler;
	unsigned int baudrate;

	clock_scaler= 0;
	prescaler = 0;
	fractional_prescaler= 0;
	baudrate=0;

	cout<<"\nenter baudrate:\t";
	cin>>baudrate;

  clock_scaler = (float) ( (float) (60 * 1000000) / (16*baudrate));
  prescaler= (unsigned int) ( ( clock_scaler - 1 ));
  fractional_prescaler = (clock_scaler - prescaler - 1) * 16;

  cout<<"\nPrescaler and Fractional Prescaler\t"<<prescaler<<"\t"<<fractional_prescaler<<endl;

  cout<< clock_scaler<<endl;
  cout<<prescaler<<endl;
  cout<<clock_scaler - prescaler -1;
	return 0;
}