#include <iostream>

using namespace std;


typedef enum
{
	noob=0,
	jackass,
	expert,
	war_veteran,
	black_ops,
	hardcore_henry,
	batman
}agent_type;


typedef struct 
{
	const char *name;
	unsigned int age;
	const char *alias;
	agent_type type;
}subject;


int main(int argc, char **argv)
{
	subject x;
	x.name="Bruce";
	x.age=24;
	x.alias="Brian doofus";
	x.type= batman;

	cout<<"\nname "<<x.name;
	cout<<"\nage "<<x.age;
	cout<<"\ntype "<<x.type;
	cout<<"\nalias "<<x.alias;
	cout<<endl;
}