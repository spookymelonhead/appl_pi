#include <iostream>

#define GIO_NUM_OF_PORTS 		(2U)


typedef enum
{
	DISABLED				= 0U,
	ENABLED,

	INVALID,
	PORT_CONFIG_NUM = INVALID

}gio_port_config_e;

typedef enum
{
	PULL_UP				= 0U,
	PULL_DOWN,

	INVALID,
	PUPD_NUM = INVALID

}gio_pupd_e;

typedef enum
{
	INPUT				= 0U,
	OUTPUT,

	INVALID,
	DIR_NUM = INVALID

}gio_dir_e;


typedef struct
{
	gio_dir_e gio_dir;

	gio_pupd_e gio_pupd;

}gio_bit_config_s;


typedef struct
{
	gio_port_config_e gio_port_config;

	gio_bit_state_s gio_bit_config[8];

}gio_port_config_s;


typedef struct
{
	gio_port_state_s gio_port_config[GIO_NUM_OF_PORTS];

}gio_config_s;


gio_config_s gio_config = 
{
	{
		/* PORT 1*/
		{
			ENABLED,
			{
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}
				{
					OUTPUT,
					PULL_UP,
				}

			}	
		}

		/* PORT 2 */
		{
			DISABLED,
			
			
			{
				DIR_INVALID,
				PUPD_INVALID,
			}
		}
	}
};

int main(int argc, char const *argv[])
{

	return 0;
}	


device_return_e gio_configure_direction(uint32_t port, uint32_t pin)
{
	gio_set_direction(port,pin, gio_config.gio_port_config[port].gio_bit_config[pin].gio_dir)
}



void gio_set_direction(uint32_t port, uint32_t pin, uint32_t dir )
{
	for(port_ctr = 0U; port_ctr< MAX_PORT; port_ctr++)
	{
		if(gio_config.gio_port_config[port].gio_port_config == ENABLED)
		{
			for(bit_ctr = 0U; bit_ctr< MAX_PIN; bit_ctr++)
			{
				gio_configure_direction(port_ctr, pin_ctr );			
			}		
		}
	}
}

