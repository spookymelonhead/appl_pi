#include <iostream>
#include <stdint.h>

typedef static mystatic;
typedef const constantine;
typedef volatile myvolatile;

typedef int (*return_integer) (int);

return_integer ret_int;

using namespace std;
int giveint(int jack)
{
	return jack;
}

int main(int argc, char const **argv)
{
	int jack = 89U;
	if(argc>1)
	{
		jack = (uint8_t) (*(*argv + 1));
		cout<<endl<<"jack equals "<<jack;
	}

	ret_int = &giveint;
	cout<<endl<<ret_int(jack)<<endl;

	cout<<"ret_int address"<<&ret_int<<endl<<&jack<<endl;

	return 0;
}