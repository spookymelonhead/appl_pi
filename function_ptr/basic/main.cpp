//https://www.youtube.com/watch?v=ynYtgGUNelE
//https://www.youtube.com/watch?v=sxTFSDAZM8s

#include <iostream>

using namespace std;


int add(int jack, int ass);

void hello(void) {cout<<"Hello"<<endl;}

void appl(void (*p) (void)){ p();}		//callback function

int main()
{
	cout<<"Sum of two nos equals\t"<<add(4,3)<<endl;

	int (*ptr)(int,int);
	ptr=&add;
	cout<<"Sum of two nos equals\t"<<(*ptr)(2,3)<<endl;


	ptr=add;
	cout<<"Sum of two nos equals\t"<<ptr(2,3)<<endl;


	void (*func_ptr)(void)=hello;
	appl(func_ptr);

	appl(hello);

	return 0;
}


int add(int jack, int ass)
{
	return jack+ass;
}