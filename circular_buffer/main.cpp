#include <iostream>
#include "stdio.h"
#include "stdint.h"
#include "stdbool.h"
#include "stdlib.h"
#include "string.h"

#define BUFFR_LENGTH (10U)

#define TRUE 1
#define FALSE 0

#define _DEBUG_EN_
using namespace std;

typedef uint16_t cbuf_data_t;

typedef struct
{
	cbuf_data_t * buffer;
	uint32_t size;

	uint32_t write, read;
	bool full;

}cbuffer_s;


/* circular buffer set to default */
bool cbuffer_set_to_default(cbuffer_s * cbuffer, uint32_t size, uint8_t resetf);

/* inits circular buffer */
bool cbuffer_init(cbuffer_s ** cbuffer, uint32_t size);

/* write data into buffer */
bool cbuffer_write(cbuffer_s * cbuffer , cbuf_data_t data);

bool cbuffer_read(cbuffer_s * cbuffer, cbuf_data_t * data_p);

bool cbuffer_write_l(cbuffer_s * cbuffer, cbuf_data_t * data_p, uint32_t size);

void cbuffer_print(cbuffer_s * cbuffer);

int main(int argc, char const *argv[])
{
	bool status = 0;
	cbuffer_s * cbuffer = NULL;
	uint16_t data = 0U;

	cbuffer_init(&cbuffer, BUFFR_LENGTH);
	cbuffer_print(cbuffer);
	cbuffer_write(cbuffer, 23U);
	cbuffer_write(cbuffer, 24U);
	cbuffer_write(cbuffer, 25U);
	cbuffer_write(cbuffer, 26U);
	cbuffer_write(cbuffer, 27U);
	cbuffer_write(cbuffer, 28U);
	cbuffer_print(cbuffer);

	cbuffer_read(cbuffer, &data);
	printf("\n%u", data);
	cbuffer_read(cbuffer, &data);
	printf("\n%u", data);
	cbuffer_print(cbuffer);

	cout<<endl;
	return 0;
}

bool is_cbuffer_empty(cbuffer_s * cbuffer)
{
	bool state = FALSE;

	if(NULL == cbuffer)
	{
		#ifdef _DEBUG_EN_
			printf("\nCircular buffer points to NULL");
		#endif /* _DEBUG_EN_ */		
		return (1);	
	}

	state = cbuffer->read == cbuffer->write;
	return (state);
}

bool is_cbuffer_full(cbuffer_s * cbuffer)
{
	bool state = FALSE;

	if(NULL == cbuffer)
	{
		#ifdef _DEBUG_EN_
			printf("\nCircular buffer points to NULL");
		#endif /* _DEBUG_EN_ */		
		return (1);	
	}

	state = cbuffer->read == ( (cbuffer->write + 1) % cbuffer->size);
	return (state);
}

bool cbuffer_init(cbuffer_s ** cbuffer_dp, uint32_t size)
{
	bool status = 0;

	if(NULL == (cbuffer_dp))	
	{
		#ifdef _DEBUG_EN_
			printf("\nArgument passed points to NULL");
		#endif /* _DEBUG_EN_ */
		status = 1;
		return (status);
	}

	if(0 == status)
	{
		*(cbuffer_dp) = (cbuffer_s *) malloc( sizeof(cbuffer_s) );
		// if memory allocation fails
		if(NULL == (*cbuffer_dp))
		{
			#ifdef _DEBUG_EN_
				printf("\nMemory allocation failed");
			#endif /* _DEBUG_EN_ */			
			status = 1;
			return (status);		
		}	
		else
		{
			(*cbuffer_dp)->buffer = (cbuf_data_t *) malloc(size  * sizeof(cbuf_data_t) );
			// if memory allocation fails
			if(NULL == (*cbuffer_dp)->buffer)	
			{
				#ifdef _DEBUG_EN_
					printf("\nMemory allocation failed");
				#endif /* _DEBUG_EN_ */							
				status = 1;
				return (status);
			}
			else
			{
				memset( (void *) (*cbuffer_dp)->buffer, 0x0, size  * sizeof(cbuf_data_t) );
			}
		}
	}
	
	if(0 == status)
	{
		status = cbuffer_set_to_default(*cbuffer_dp, size, FALSE);
  }

  return (status);
}

bool cbuffer_set_to_default(cbuffer_s * cbuffer, uint32_t size, uint8_t resetf)
{
	bool status = 0;
	if(NULL == cbuffer)
	{
		#ifdef _DEBUG_EN_
			printf("\nCircular buffer points to NULL");
		#endif /* _DEBUG_EN_ */
		status = 1;		
		return (status);	
	}

	if(TRUE == resetf)
	{
		free(cbuffer->buffer);
	}
	else
	{
		// do nothing
	}

	cbuffer->size = size; 
	cbuffer->write = 0;
	cbuffer->read = 0;
	cbuffer->full = 0;

	#ifdef _DEBUG_EN_
		printf("\nCircular Buffer initialized to default");
	#endif /* _DEBUG_EN_ */

	return (status);
}

bool cbuffer_write(cbuffer_s *cbuffer , cbuf_data_t data)
{
	bool status = 0;
	
	if(NULL == cbuffer)
	{
		#ifdef _DEBUG_EN_
			printf("\nCircular buffer points to NULL");
		#endif /* _DEBUG_EN_ */		
		return (status);	
	}

	cbuffer->buffer[cbuffer->write] = data;
	cbuffer->write = (cbuffer->write + 1U) % cbuffer->size;
	if(cbuffer->write == cbuffer->read)
	{
		cbuffer->read = (cbuffer->read + 1U) % cbuffer->size;
	}
	return (status);
}

bool cbuffer_read(cbuffer_s *cbuffer , cbuf_data_t * data_p)
{
	bool status = 0;

	if(NULL == cbuffer)
	{
		#ifdef _DEBUG_EN_
			printf("\nCircular buffer points to NULL");
		#endif /* _DEBUG_EN_ */		
		return (status);	
	}
	if(TRUE == is_cbuffer_empty(cbuffer) )
	{
		#ifdef _DEBUG_EN_
		printf("\ncBuffer empty");
		#endif /* _DEBUG_EN_ */		
		return 0;
	}

	if(NULL ==  data_p)
	{
		#ifdef _DEBUG_EN_
			printf("\nArgument passed points to NULL");
		#endif /* _DEBUG_EN_ */
		status = 1;
		return (status);
	}

	if(0 == status)
	{
		#ifdef _DEBUG_EN_
			printf("\nData read");
		#endif /* _DEBUG_EN_ */
		*data_p = cbuffer->buffer[cbuffer->read];
		cbuffer->read = (cbuffer->read + 1U) % cbuffer->size;
	}
	return (status);
}

void cbuffer_print(cbuffer_s *cbuffer)
{
	uint32_t counter = 0U;
	if(NULL == cbuffer)
		return;

		printf("\ncBuffer:");
		printf("\n\tSize @ %u Read @ %u Write @ %u \t", cbuffer->size, cbuffer->read, cbuffer->write);
	if(cbuffer->full)
		printf("Full");
	else
		printf("NotFull");

	printf("\ncBuffer's content\n\t");
	for(counter = 0;  counter < cbuffer->size ; counter++)
	{
		cout<<cbuffer->buffer[counter];
		cout<<"  ";
	}
}

#ifdef JACK
bool into_cbuffr_l(cbuffer_s *cbuffr ,unsigned int *data_ptr, unsigned int data_sizegth, unsigned int buffer_size)
{
	bool tintin = 0;

	if(((cbuffr->write + 1) % buffer_size ) != cbuffr->read)
	{
		while(data_sizegth)
		{
			cbuffr->buffer[cbuffr->write] = data_ptr[data_sizegth];
			if(0 == cbuffr->full)
			cbuffr->write = (cbuffr->write + 1) % buffer_size;
			data_sizegth--;
		}
	}

	else  
		tintin = 1;

	return tintin;
}

bool outfrom_cbuffr(cbuffer_s *cbuffr , unsigned int * data_ptr, unsigned int buffer_size)
{
	bool tintin = 0;

	if(NULL ==  data_ptr)
	{
		cout<<"\nNULL pointer..in your face";
		tintin = 1;
		return tintin;
	}

	if(cbuffr->write != cbuffr->read)
	{
		*data_ptr =  cbuffr->buffer[cbuffr->read];
		cbuffr->buffer[cbuffr->read] = 0;
		cbuffr->read = (cbuffr->read + 1) % buffer_size;
	}

	else
		tintin = 1;

	return tintin;
}


bool read_pop_cbuffr(cbuffer_s *cbuffr, unsigned int buffer_size)
{
	unsigned int datass = 0U;
	bool tintin = 0;

	tintin = outfrom_cbuffr(cbuffr, &datass,buffer_size);
	
	if(!tintin)
		cout<<"\nPopped element: "<<datass;
		cout<<endl;

	return tintin;
}

#endif 